package com.bliffoscope;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;

public class GenerateInput {

	public static void main(String[] args) throws IOException {
		boolean[][] arr = new boolean[500][500];
	    Random r = new Random();

	    for(int i = 0; i < arr.length; i++){
	        for(int j = 0; j < arr[i].length; j++){

	            arr[i][j] = r.nextBoolean();

	            //System.out.print(arr[i][j]+"\t");
	        }
	    }   
	    Path path = Paths.get("output.txt");
	    
	  //Use try-with-resource to get auto-closeable writer instance
	  try (BufferedWriter writer = Files.newBufferedWriter(path))
	  {
	      writer.write(Arrays.deepToString(arr));
	  }
	    //System.out.println(Arrays.deepToString(arr));
	}

}

/*
 
		int inputX = matchRequest.getInput().length;
		int inputY = matchRequest.getInput()[0].length;
		int patternX = matchRequest.getPattern().length;
		int patternY = matchRequest.getPattern()[0].length;
		
		int tmpIX = 0;
		boolean moveRight = true;
		
		while((tmpIX+patternX) <= inputX) {
			if(moveRight) {
				int tmpIY = patternY;
				while(tmpIY <= inputY) {
					//System.out.println("tmpIX = " + tmpIX + "," + "tmpIY = " + tmpIY);
					int errorVerticle = 0;
					int errorVerticleInverted = 0;
					for(int x = tmpIX; x< patternX+tmpIX; x++) {
						for(int y = tmpIY-patternY; y < tmpIY; y++) {
							// System.out.println(matchRequest.getInput()[x][y] + "\t" + matchRequest.getPattern()[x-tmpIX][patternY-(tmpIY-y-1)-1]);
							if(matchRequest.getInput()[x][y] != matchRequest.getPattern()[x-tmpIX][patternY-(tmpIY-y-1)-1]) {
								errorVerticle++;
							}
							// System.out.println(x + " : " + y + " , " + (patternX-(x-tmpIX)-1)+ " : " + (tmpIY-y-1) + "||" + matchRequest.getInput()[x][y] + " - " + 
								// 	matchRequest.getPattern()[patternX-(x-tmpIX)-1][tmpIY-y-1]);
							if(matchRequest.getInput()[x][y] != matchRequest.getPattern()[patternX-(x-tmpIX)-1][tmpIY-y-1]) {
								errorVerticleInverted++;
							}
						}
						// System.out.println();
					}
					tmpIY++;
					if(errorVerticle < matchRequest.getTolerance()) {
						System.out.println("****** FOUND MATCH @ " + tmpIX + " - " + (tmpIY-patternY-1) + ", " + errorVerticle);
					} 
					if(errorVerticleInverted < matchRequest.getTolerance()) {
						System.out.println("****** FOUND INVERTED MATCH @ " + tmpIX + " - " + (tmpIY-patternY) + ", " + errorVerticleInverted);
					}
				}
				moveRight = false;
			} else {
				// System.out.println("==================================================================");
				int tmpIY = inputY - patternY;
				while(tmpIY > 0) {
					//System.out.println("tmpIX = " + tmpIX + "," + "tmpIY = " + tmpIY);
					int errorVerticle = 0;
					int errorVerticleInverted = 0;
					for(int x = tmpIX; x< patternX+tmpIX; x++) {
						for(int y = tmpIY-1; y < tmpIY + patternY - 1; y++) {
							// System.out.print(matchRequest.getInput()[x][y] + "\t");
							if(matchRequest.getInput()[x][y] != matchRequest.getPattern()[x-tmpIX][y-tmpIY+1]) {
								errorVerticle++;
							}
							//System.out.println( (patternX-(x-tmpIX)-1 )+ "\t" + (patternY-(y-tmpIY+1)-1));
							if(matchRequest.getInput()[x][y] != matchRequest.getPattern()[patternX-(x-tmpIX)-1][patternY-(y-tmpIY+1)-1]) {
								errorVerticleInverted++;
							}
						}
						// System.out.println();
					}
					tmpIY--;
					if(errorVerticle < matchRequest.getTolerance()) {
						System.out.println("****** FOUND MATCH in right to left @ " + tmpIX + " - " + (tmpIY-patternY)+ ", " + errorVerticle);
					} 
					if(errorVerticleInverted < matchRequest.getTolerance()) {
						System.out.println("****** FOUND INVERTED MATCH in right to left @ " + tmpIX + " - " + tmpIY + ", " + errorVerticleInverted);
					}
				}
				moveRight = true;
			}
			tmpIX++;
		}
		
		return "1234";
	
 */ 
