package com.bliffoscope;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BliffoscopeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BliffoscopeServiceApplication.class, args);
	}
}
