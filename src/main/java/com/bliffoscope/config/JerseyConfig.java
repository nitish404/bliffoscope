package com.bliffoscope.config;


import javax.inject.Named;
import javax.ws.rs.Path;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.bliffoscope.BliffoscopeServiceApplication;

/**
 * Configures Jersey for use with Spring Boot.
 */
@Named
@Configuration
@ComponentScan("com.bliffoscope.controller.*")
@EntityScan("com.bliffoscope.service.*")
public class JerseyConfig extends ResourceConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(JerseyConfig.class);
			
	public JerseyConfig() {
		register(CORSResponseFilter.class);
		packages(BliffoscopeServiceApplication.class.getPackage().toString());
	}
}