package com.bliffoscope.controller;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import com.bliffoscope.model.MatchRequestPayload;
import com.bliffoscope.service.BliffoscopeService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Path("bliffo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Controller
public class BliffoscopeController {
	
	private static final Logger logger = LoggerFactory.getLogger(BliffoscopeController.class);
	
	@Autowired(required = true)
	private BliffoscopeService bliffoscopeService;
	
	@POST
	@Path("match")
	public Response findMatches(MatchRequestPayload matchRequest)
			throws JSONException, JsonParseException, JsonMappingException, IOException {
		logger.info(matchRequest.toString());
		return Response.ok(bliffoscopeService.computeMatches(matchRequest)).build();
	}
}
