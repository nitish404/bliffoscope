package com.bliffoscope.service;

import java.util.List;

import com.bliffoscope.model.GridLocationEvaluation;
import com.bliffoscope.model.InputType;
import com.bliffoscope.strategy.IStrategy;

public class PatternMatcher {

	private int tolerance;
	private InputType[][] input;
	private InputType[][] pattern;
	
	public PatternMatcher(InputType[][] input, InputType[][] pattern, int tolerance) {
		this.input = input;
		this.pattern = pattern;
		this.tolerance = tolerance;
	}
	
	public List<GridLocationEvaluation> matchPattern(IStrategy strategy){
		return strategy.matchPattern(input, pattern, tolerance);
	}
}
