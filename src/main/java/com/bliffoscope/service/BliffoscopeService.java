package com.bliffoscope.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bliffoscope.model.GridLocationEvaluation;
import com.bliffoscope.model.MatchRequestPayload;
import com.bliffoscope.strategy.BruteForceStrategy;
import com.bliffoscope.strategy.IStrategy;
import com.bliffoscope.strategy.SumStrategy;

@Service
public class BliffoscopeService {

	public List<GridLocationEvaluation> computeMatches(MatchRequestPayload matchRequest) {
		IStrategy strategy = null;
		if(matchRequest.getStrategy().equals("sum")) {
			strategy = new SumStrategy();
		} else {
			strategy = new BruteForceStrategy();
		}
		//for some RGB strategy invoke corresponding strategy which might work on RGBPixel
		
		PatternMatcher patternMatcher = new PatternMatcher(matchRequest.getInput(), 
				matchRequest.getPattern(), matchRequest.getTolerance());
		return patternMatcher.matchPattern(strategy);
	}
}
