package com.bliffoscope.model;

import com.bliffoscope.model.types.GridLocation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GridLocationEvaluation {

	private GridLocation gridLocation;
	private int errors;
	
	public GridLocationEvaluation(GridLocation gridLocation, int errors) {
		this.gridLocation = gridLocation;
		this.errors = errors;
	}
	
	@JsonProperty(value="gridLocation")
	public GridLocation getGridLocation() {
		return gridLocation;
	}
	
	@JsonProperty(value="gridLocation")
	public void setGridLocation(GridLocation gridLocation) {
		this.gridLocation = gridLocation;
	}
	
	@JsonProperty(value="errors")
	public int getErrors() {
		return errors;
	}
	
	@JsonProperty(value="errors")
	public void setErrors(int errors) {
		this.errors = errors;
	}
	
	
}
