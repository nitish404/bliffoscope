package com.bliffoscope.model.typefactory;

import java.util.LinkedHashMap;
import java.util.Map;

import com.bliffoscope.model.InputType;
import com.bliffoscope.model.types.BooleanPixel;
import com.bliffoscope.model.types.GridLocation;
import com.bliffoscope.model.types.IPixel;
import com.bliffoscope.model.types.RGBPixel;

public class FlyweigthRGBPixelFactory {
	
	private Map<GridLocation, IPixel<Object>> inputPixelWindow;
	
	private InputType[][] input;
	
	public FlyweigthRGBPixelFactory(InputType[][] input, int windowSizeX, int windowSizeY) {
		this.input = input;
		
		inputPixelWindow = new LinkedHashMap<>(windowSizeX*windowSizeY);
		
		//initialize the inputWindow with starting from 0,0
		for(int i=0; i < windowSizeX; i++) {
			for(int j=0; j < windowSizeY; j++) {
				GridLocation gridLocation = new GridLocation(i, j);
				inputPixelWindow.put(gridLocation, new RGBPixel());
			}
		}
	}
	
	public Map<GridLocation, IPixel<Object>> getInputPixelWindow(int xOffset, int yOffset){
		// TODO - not implemented
		return inputPixelWindow;
	}
	
}
