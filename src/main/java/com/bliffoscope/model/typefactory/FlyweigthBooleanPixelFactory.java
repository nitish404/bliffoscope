package com.bliffoscope.model.typefactory;

import java.util.LinkedHashMap;
import java.util.Map;

import com.bliffoscope.model.InputType;
import com.bliffoscope.model.types.BooleanPixel;
import com.bliffoscope.model.types.GridLocation;
import com.bliffoscope.model.types.IPixel;

public class FlyweigthBooleanPixelFactory {
	
	private Map<GridLocation, IPixel<Boolean>> inputPixelWindow;
	
	private InputType[][] input;
	
	private  int windowSizeX; 
	private int windowSizeY;
	
	public FlyweigthBooleanPixelFactory(InputType[][] input, int windowSizeX, int windowSizeY) {
		this.input = input;
		this.windowSizeX = windowSizeX;
		this.windowSizeY = windowSizeY;
		
		inputPixelWindow = new LinkedHashMap<>(windowSizeX*windowSizeY);
		
		//initialize the inputWindow with starting from 0,0
		for(int i=0; i < windowSizeX; i++) {
			for(int j=0; j < windowSizeY; j++) {
				GridLocation gridLocation = new GridLocation(i, j);
				inputPixelWindow.put(gridLocation, new BooleanPixel((boolean) input[i][j].getValue()));
			}
		}
	}
	
	public Map<GridLocation, IPixel<Boolean>> getInputPixelWindow(int xOffset, int yOffset) {
		inputPixelWindow.forEach((gridlocation, booleanPixel) -> {
			int newYPosition = gridlocation.getY() + yOffset;
			if(newYPosition+windowSizeY == input[0].length) {
				newYPosition = 0;
			}
			gridlocation.setX(xOffset);
			gridlocation.setY(newYPosition);
			
			booleanPixel.setValue((Boolean) input[xOffset][newYPosition].getValue());
		});
		return inputPixelWindow;
	}
	
}
