package com.bliffoscope.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName(value="MatchRequest")
public class MatchRequestPayload {

	private String strategy;
	
	private InputType[][] input;
	
	private InputType[][] pattern;
	
	private int tolerance;
	
	@JsonProperty(value="strategy")
	public String getStrategy() {
		return strategy;
	}

	@JsonProperty(value="strategy")
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	@JsonProperty(value="input")
	public InputType[][] getInput() {
		return input;
	}

	@JsonProperty(value="input")
	public void setInput(InputType[][] input) {
		this.input = input;
	}

	@JsonProperty(value="pattern")
	public InputType[][] getPattern() {
		return pattern;
	}

	@JsonProperty(value="pattern")
	public void setPattern(InputType[][] pattern) {
		this.pattern = pattern;
	}
	
	public int getTolerance() {
		return tolerance;
	}

	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}

	@Override
	public String toString() {
		return strategy + "\n" + Arrays.deepToString(input) + "\n" + Arrays.deepToString(pattern);
	}
	
}
