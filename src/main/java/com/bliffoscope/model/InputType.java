package com.bliffoscope.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InputType {

	private Object value;

	@JsonProperty(value="value")
	public Object getValue() {
		return value;
	}

	@JsonProperty(value="value")
	public void setValue(Object value) {
		this.value = value;
	}
	
}
