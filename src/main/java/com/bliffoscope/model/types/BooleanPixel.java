package com.bliffoscope.model.types;

public class BooleanPixel implements IPixel<Boolean> {
	
	private boolean value;

	public BooleanPixel(boolean value) {
		this.value = value;
	}
	
	@Override
	public Boolean getValue() {
		return value;
	}

	@Override
	public void setValue(Boolean value) {
		this.value = value;
	}
	
	@Override
	public int compareTo(IPixel<Boolean> o) {
		if(o.getValue() == value)
			return 0;
		else					
			return 1;
	}

}
