package com.bliffoscope.model.types;

public interface IPixel<T> extends Comparable<IPixel<T>>{
	
	T getValue();
	
	void setValue(T value);
}
