package com.bliffoscope.strategy;

import java.util.List;

import com.bliffoscope.model.GridLocationEvaluation;
import com.bliffoscope.model.InputType;

public interface IStrategy {

	List<GridLocationEvaluation> matchPattern(InputType input[][], 
			InputType[][] pattern, int tolerance);
}
