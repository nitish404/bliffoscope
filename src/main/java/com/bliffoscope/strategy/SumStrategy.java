package com.bliffoscope.strategy;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.bliffoscope.model.GridLocationEvaluation;
import com.bliffoscope.model.InputType;
import com.bliffoscope.model.typefactory.FlyweigthBooleanPixelFactory;
import com.bliffoscope.model.types.BooleanPixel;
import com.bliffoscope.model.types.GridLocation;
import com.bliffoscope.model.types.IPixel;

public class SumStrategy implements IStrategy {

	private AtomicInteger patternSum = new AtomicInteger(0);
	
	private List<GridLocationEvaluation> patternmatchResults = new LinkedList<>();
	
	@Override
	public List<GridLocationEvaluation> matchPattern(InputType input[][], 
			InputType pattern[][], int tolerance) {

		Arrays.stream(pattern)
	    .flatMap(Arrays::stream)
	        .forEach((patternInput) -> {
	        	if((boolean) patternInput.getValue()) {
	        		patternSum.incrementAndGet();
	        	}
	        });
		
		FlyweigthBooleanPixelFactory inputPixelFactory = new FlyweigthBooleanPixelFactory(input, 
				pattern.length, pattern[0].length);
		
		for(int i=0; i < (input.length-pattern.length); i++) {
			
			for(int j=0; j < (input[i].length-pattern[0].length); j++) {
				Map<GridLocation, IPixel<Boolean>> inputPixelWindow = 
						inputPixelFactory.getInputPixelWindow(i, 1);
				
				AtomicInteger sumInCurrentWindow = new AtomicInteger(0);
				inputPixelWindow.forEach((gridlocation, booleanPixel) -> {
					if(booleanPixel.getValue()) {
						sumInCurrentWindow.incrementAndGet();
					}
				});
				if(Math.abs(patternSum.get()-sumInCurrentWindow.get()) <= tolerance) {
					patternmatchResults.add(new GridLocationEvaluation(new GridLocation(i, j), 
							Math.abs(patternSum.get()-sumInCurrentWindow.get())));
				}
			}
		}
		return patternmatchResults;
	
	}

}
